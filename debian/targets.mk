# Target architectures supported by u-boot in Debian.
# debian/rules includes this Makefile snippet.

ifeq (${DEB_HOST_ARCH},arm64)

# u-boot-asahi

  # NoisyCoil <noisycoil@tutanota.com>
  u-boot-asahi_platforms += apple_m1
  apple_m1_targets := u-boot-nodtb.bin

endif
